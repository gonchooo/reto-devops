FROM node:erbium-alpine3.11
RUN apk update && apk add --no-cache curl
ARG	REPO_HOME=/var/app/
ARG MYUSER=goncho
ARG	MYGROUP=appgroup 	
RUN	addgroup -S ${MYGROUP} && adduser -s /bin/bash -S -G ${MYGROUP} ${MYUSER} && \
	mkdir -p ${REPO_HOME} && chown ${MYUSER}:${MYGROUP} ${REPO_HOME}
WORKDIR ${REPO_HOME}
USER ${MYUSER} 
COPY ./ ./
RUN npm install && npm audit fix
EXPOSE 3000
ENTRYPOINT ["node", "index.js"]